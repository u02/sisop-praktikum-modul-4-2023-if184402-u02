<h1>Praktikum Modul 4 Sistem Operasi</h1>
<h3>Group U02</h3>

| NAME                      | NRP       |
|---------------------------|-----------|
|Pascal Roger Junior Tauran |5025211072 |
|Riski Ilyas                |5025211189 |
|Armstrong Roosevelt Zamzami|5025211191 |


## Number 1
```
Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline #YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪) sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran. 
Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.
Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.
```
### 1A

```
Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.

kaggle datasets download -d bryanb/fifa-player-stats-database

Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya.
```
Solution:<br>
```c
void downloadDataset() {
    const char* downloadCommand = "kaggle datasets download -d bryanb/fifa-player-stats-database";
    system(downloadCommand);
}

void extractDataset() {
    const char* unzipCommand = "unzip fifa-player-stats-database.zip";
    system(unzipCommand);
}

int main() {
    downloadDataset();
    extractDataset();
    return 0;
}
```
Explanation:
- `downloadDataset()` function downloads the dataset from Kaggle using the `system()` function to execute a command-line command.
The command being executed is stored in the `downloadCommand` variable and is set to `kaggle datasets download -d bryanb/fifa-player-stats-database`.
- `extractDataset()` function extracts the downloaded dataset. Similar to `downloadDataset()`, it uses the `system()` function to execute a command-line command. The command being executed is stored in the `unzipCommand` variable and is set to `unzip` `fifa-player-stats-database.zip`.
- The `main` function then calls the two functions to be executed.

### 1B
```
Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.
```
Solution:<br>
```c
void readCSV(const char* filename) {
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("File is not found!\n");
        return;
    }

    // Read the header line
    char header[MAX_LINE_LENGTH];
    fgets(header, sizeof(header), file);

    // Read and process each line of the CSV file
    char line[MAX_LINE_LENGTH];
    while (fgets(line, sizeof(line), file)) {
        char* token = strtok(line, ",");
        int column = 1;
        Player player;

        while (token != NULL && column <= MAX_COLUMNS) {
            switch (column) {
                case 1:
                    player.id = atoi(token);
                    break;
                case 2:
                    player.name = token;
                    break;
                case 3:
                    player.age = atoi(token);
                    break;
                case 4:
                    player.photoURL = token;
                    break;
                case 5:
                    player.nationality = token;
                    break;
                case 8:
                    player.potential = atoi(token);
                    break;
                case 9:
                    player.club = token;
                    break;
            }

            token = strtok(NULL, ",");
            column++;
        }

        if (player.age < 25 && player.potential > 85 && isInterestedClub(player.club)) {
            printf("Id: %d\n", player.id);
            printf("Name: %s\n", player.name);
            printf("Age: %d\n", player.age);
            printf("Photo: %s\n", player.photoURL);
            printf("Nationality: %s\n", player.nationality);
            printf("Potential: %d\n", player.potential);
            printf("Club: %s\n", player.club);
            printf("----------------------------------\n");
        }
    }

    fclose(file);
}

int main() {
    downloadDataset();
    extractDataset();

    // Create a child process to execute readCSV
    pid_t child_pid = fork();

    if (child_pid == -1) {
        // Error occurred
        perror("Fork failed");
        exit(EXIT_FAILURE);
    } else if (child_pid == 0) {
        // Child process executes readCSV
        readCSV("FIFA23_official_data.csv");
        exit(EXIT_SUCCESS);
    } else {
        // Parent process waits for the child process to complete
        int status;
        waitpid(child_pid, &status, 0);
    }

    return 0;
}
```
Explanation:
- `readCSV()`function reads and processes a CSV file containing FIFA player statistics. The function reads the header line of the CSV file using `fgets()` and stores it in the header array. It enters a loop to read and process each line of the CSV file.
Each line is read using `fgets()` and stored in the line array. The line is then tokenized using `strtok()` to extract individual values separated by commas. The values are assigned to the corresponding fields of a Player structure (player).
The `switch` statement is used to assign the values to the correct fields based on the column number.
After processing each line, if certain conditions are met `player.age` < 25, `player.potential > 85`, and `isInterestedClub(player.club)`, player information is printed using `printf()`.
- `main()` calls the `downloadDataset()` and `extractDataset()` functions to download and extract the dataset. Then, it creates a child process using `fork()` to execute the readCSV() function. This is done so that the `readCSV` fucntion waits for the `downloadDataset` and `extractDataset` functions to finish execution before starting. If `fork()` succeeds and the current process is the child process `child_pid == 0`, it calls `readCSV()` with the filename `FIFA23_official_data.csv`. After the child process completes, it exits with success.

### 1C

```
Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.
Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.
```
Solution:<br>
```dockerfile
FROM alpine:latest

WORKDIR /app

COPY . /app

RUN apk update && apk add build-base

RUN gcc -o storage storage.c

CMD ["./storage"]
```
Explanation:
- `FROM alpine:latest` command specifies the base image for the Docker image which is linux Alpine in this case, this was chosen as it was very light and did not consume as much storage as other linux distributions.
- `WORKDIR /app` sets the working directory inside the container to `/app`. It creates the directory if it doesn't exist and sets it as the default location for subsequent commands to be executed.
- `COPY . /app` copies the files from the current directory where the Dockerfile is located into the `/app` directory within the container. The `.` indicates that all files and directories in the current directory will be copied. 
- `RUN apk update && apk add build-base`  updates the package repository inside the container and then installs the `build-base` package, which includes essential build tools such as the `C compiler` and `libraries`.
- `RUN gcc -o storage storage.c` compiles the C program `storage.c` using the gcc compiler. `-o` specifies the name of the output executable as `storage`. And the resulting executable will be placed in the same directory as the Dockerfile, which is `/app` inside the container.
- `CMD ["./storage"]` runs the storage executable compiled in the previous step. In this case the executable `storage`  will be executed and thus will run the program.

### 1D

```
Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?
Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.
```
Solution:<br>
```
https://hub.docker.com/r/pascalrjt/storage-app
```
Explanation:
- The container was uploaded to docker hub using the `docker push` command in the terminal
- The docker image was first tagged using the command `docker tag <image_name>:<tag> <dockerhub_username>/<image_name>:<tag>` where in this case the tag will be `YBBA`.
- After logging into docker hub through ther terminal using the command `docker login` we push the tagged docker image to dockerhub using the command `docker push <dockerhub_username>/<image_name>:<tag>` where the tag is `YBBA`.


### 1E

```
Berita tagline #YBBA (#YangBiruBiruAja) semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.
```
Solution:<br>
```yml
version: "3"
services:
  napoli:
    build: 
      context: /home/pascal/Documents/praktikum/modul4/soal1/Napoli
      dockerfile: /home/pascal/Documents/praktikum/modul4/soal1/docker1/Dockerfile
    deploy:
      replicas: 5
  barcelona:
    build:
      context: /home/pascal/Documents/praktikum/modul4/soal1/Barcelona
      dockerfile: /home/pascal/Documents/praktikum/modul4/soal1/docker1/Dockerfile
    deploy:
      replicas: 5
```
Exlpanation:
- `services` defines the individual servies that will be deployed.
- `napoli` and `barcelona` are services that will be deployed.
- `build` specifies the build directory which in this case is `/home/pascal/Documents/praktikum/modul4/soal1/Napoli` for the `napoli` service and `/home/pascal/Documents/praktikum/modul4/soal1/Barcelona` for the `barcelona` service.
- `dockerfile` specifies the dockerfile direcory which in this case is `/home/pascal/Documents/praktikum/modul4/soal1/docker1/Dockerfile` for both services.
- `deploy` is used to define deployment-related configurations.
- `replicas: 5` specifies that 5 replicas of the `napoli` and `barcelona` service should be created.

## Number4
```
Pada suatu masa, terdapat sebuah perusahaan bernama Bank Sabar Indonesia yang berada pada masa kejayaannya. Bank tersebut memiliki banyak sekali kegiatan-kegiatan yang  krusial, seperti mengolah data nasabah yang mana berhubungan dengan uang. Suatu ketika, karena susahnya maintenance, petinggi bank ini menyewa seseorang yang mampu mengorganisir file-file yang ada di Bank Sabar Indonesia. 
Salah satu karyawan di bank tersebut merekomendasikan Bagong sebagai seseorang yang mampu menyelesaikan permasalahan tersebut. Bagong memikirkan cara terbaik untuk mengorganisir data-data nasabah dengan cara membagi file-file yang ada dalam bentuk modular, yaitu membagi file yang mulanya berukuran besar menjadi file-file chunk yang berukuran kecil. Hal ini bertujuan agar saat terjadi error, Bagong dapat mudah mendeteksinya. Selain dari itu, agar Bagong mengetahui setiap kegiatan yang ada di filesystem, Bagong membuat sebuah sistem log untuk mempermudah monitoring kegiatan di filesystem yang mana, nantinya setiap kegiatan yang terjadi akan dicatat di sebuah file log dengan ketentuan sebagai berikut.
```
## 4A
```
Pada filesystem tersebut, jika Bagong membuat atau me-rename sebuah direktori dengan awalan module_, maka direktori tersebut akan menjadi direktori modular. 
```
Solution:<br>

```c
//No current solution
```

Explanation:
- `No current explanation` 


## 4B
```
Bagong menginginkan agar saat melakukan modularisasi pada suatu direktori, maka modularisasi tersebut juga berlaku untuk konten direktori lain di dalam direktori (subdirektori) tersebut.
```
Solution:<br>

```c
//No current solution
```

Explanation:<br>
- `No current explanation`
### 4C
```
Sebuah file nantinya akan terbentuk bernama fs_module.log pada direktori home pengguna (/home/[user]/fs_module.log) yang berguna menyimpan daftar perintah system call yang telah dijalankan dengan sesuai dengan ketentuan yang telah disebutkan sebelumnya.
```
Solution:<br>

```c
//No current solution
```
Explanation:<br>
- `No current explanation`

### 4D
```
Saat Bagong melakukan modularisasi, file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar 1024 bytes dan menjadi normal ketika diakses melalui filesystem yang dirancang oleh dia sendiri.
```
Solution:<br>
```c
//No current solution
```
Explanation:
- `No current explanation`

### 4E
```
Apabila sebuah direktori modular di-rename menjadi tidak modular, maka isi atau konten direktori tersebut akan menjadi utuh kembali (fixed).
```
Solution:<br>
```c
//No current solution
```
Explanation:
- `No current explanation`


## Number5
```
Setelah sukses menjadi pengusaha streaming musik di chapter kehidupan sebelumnya, Elshe direkrut oleh lembaga rahasia untuk membuat sistem rahasia yang terenkripsi.  Kalian perlu membantu Elshe dan membuat program rahasia.c. Pada program rahasia.c, terdapat beberapa hal yang harus kalian lakukan sebagai berikut.
```

### 5A

```
Program rahasia.c merupakan file FUSE yang akan digunakan untuk melakukan mount folder pada Docker Container. Unduh file rahasia.zip kemudian lakukan unzip pada file rahasia.zip menjadi folder rahasia.
```
Solution:<br>
```c

void download_n_extract() {
    const char* downloadLink = "https://drive.google.com/uc?export=download&id=18YCFdG658SALaboVJUHQIqeamcfNY39a&confirm=t&uuid=38b0d9df-d8cc-46c8-9f03-b455b6f12ed9&at=AKKF8vzt0K3vcrmKQYVrFdCQpjzY:1685177895570";
    const char* fileName = "rahasia.zip";

    // Download the file using wget
    char downloadCommand[256];
    snprintf(downloadCommand, sizeof(downloadCommand), "wget -O %s \"%s\"", fileName, downloadLink);
    int downloadStatus = system(downloadCommand);
    if (downloadStatus != 0) {
        printf("Failed to download the file.\n");
        return;
    }

    // Extract the downloaded file using unzip
    char extractCommand[256];
    snprintf(extractCommand, sizeof(extractCommand), "unzip %s", fileName);
    int extractStatus = system(extractCommand);
    if (extractStatus != 0) {
        printf("Failed to extract the file.\n");
        return;
    }

    printf("File downloaded and extracted successfully.\n");
}

```
Exlpanation:

- `void download_n_extract()`: This is the function declaration for a function named `download_n_extract()`. It doesn't take any parameters and doesn't return any value.
- `const char* downloadLink = "https://drive.google.com/uc?export=download&id=18YCFdG658SALaboVJUHQIqeamcfNY39a&confirm=t&uuid=38b0d9df-d8cc-46c8-9f03-b455b6f12ed9&at=AKKF8vzt0K3vcrmKQYVrFdCQpjzY:1685177895570";`: This line declares a constant character pointer `downloadLink` and assigns it a value of a download link. The link is a Google Drive download link.
- `const char* fileName = "rahasia.zip";`: This line declares a constant character pointer `fileName` and assigns it a value of the desired file name to be downloaded and extracted, in this case, "rahasia.zip".
- `char downloadCommand[256];`: This line declares a character array named `downloadCommand` with a size of 256 characters. It will be used to store a command string.
- `snprintf(downloadCommand, sizeof(downloadCommand), "wget -O %s \"%s\"", fileName, downloadLink);`: This line uses the `snprintf` function to format and store a command string in the `downloadCommand` array. The command string is a `wget` command with the `-O` flag to specify the output file name, and it includes the `fileName` and `downloadLink` values.
- `int downloadStatus = system(downloadCommand);`: This line executes the command stored in `downloadCommand` using the `system()` function. It attempts to download the file specified by the `downloadLink` and saves it with the provided `fileName`. The return value of `system()` is stored in `downloadStatus`, which represents the status of the executed command.
- `if (downloadStatus != 0) { printf("Failed to download the file.\n"); return; }`: This block of code checks the value of `downloadStatus`. If it is not equal to 0, it means that the `system()` function call failed to execute the download command successfully. In such a case, an error message is printed, and the function returns early.
- `char extractCommand[256];`: This line declares a character array named `extractCommand` with a size of 256 characters. It will be used to store a command string.
- `snprintf(extractCommand, sizeof(extractCommand), "unzip %s", fileName);`: This line uses the `snprintf` function to format and store a command string in the `extractCommand` array. The command string is an `unzip` command followed by the `fileName`, specifying the file to be extracted.
- `int extractStatus = system(extractCommand);`: This line executes the command stored in `extractCommand` using the `system()` function. It attempts to extract the file specified by the `fileName` using the `unzip` command. The return value of `system()` is stored in `extractStatus`, representing the status of the executed command.
- `if (extractStatus != 0) { printf("Failed to extract the file.\n"); return; }`: This block of code checks the value of `extractStatus`. If it is not equal to 0, it means that the `system()` function call failed to execute the extraction command successfully. In such a case, an error message is printed, and the function returns early.
- `printf("File downloaded and extracted successfully.\n");`: If the download and extraction process completes without any errors, this line prints a success message indicating that the file was downloaded and extracted successfully.

### 5B
```
Seperti soal a, folder rahasia akan di-mount pada Docker Container dengan image bernama rahasia_di_docker_<Kode Kelompok> pada direktori /usr/share. Gunakan Ubuntu Focal Fossa sebagai base image pada Dockerfile.
```
Solution:<br>
```c

void mount() {
    system("sudo docker run -it --name temp_container ubuntu:focal");
    system("apt-get update");
    system("sudo docker commit temp_container rahasia_di_docker_u02");
    system("exit");
    system("sudo docker run -v $(pwd)/rahasia:/usr/share/rahasia -it rahasia_di_docker_u02");
}

```
Explanation:

- `void mount()`: This is the function declaration for a function named `mount()`. It doesn't take any parameters and doesn't return any value.
- `system("sudo docker run -it --name temp_container ubuntu:focal");`: This line uses the `system()` function to execute a shell command. It runs a Docker container based on the `ubuntu:focal` image and assigns it the name `temp_container`. The `-it` flags are used to run the container interactively, allowing user input and output.
- `system("apt-get update");`: This line executes the shell command `apt-get update` inside the Docker container. It updates the package lists for available software packages.
- `system("sudo docker commit temp_container rahasia_di_docker_u02");`: This line executes the shell command `sudo docker commit temp_container rahasia_di_docker_u02`. It creates a new Docker image named `rahasia_di_docker_u02` by committing the changes made in the `temp_container` container.
- `system("exit");`: This line executes the shell command `exit`. It is likely used to exit the current shell session inside the Docker container.
- `system("sudo docker run -v $(pwd)/rahasia:/usr/share/rahasia -it rahasia_di_docker_u02");`: This line executes the shell command `sudo docker run -v $(pwd)/rahasia:/usr/share/rahasia -it rahasia_di_docker_u02`. It runs a new Docker container based on the `rahasia_di_docker_u02` image. The `-v` flag is used to mount the `$(pwd)/rahasia` directory from the host machine to the `/usr/share/rahasia` directory inside the container. The `-it` flags enable interactive mode for the container.


### 5C
```
Setelah melakukan mount, buatlah register system yang menyimpan kredensial berupa username dan password. Agar lebih aman, password disimpan dengan menggunakan hashing MD5. Untuk mempermudah kalian, gunakan system()  serta gunakan built-in program untuk melakukan hashing MD5 pada Linux (tidak wajib). Username dan password akan disimpan dengan format <username>;<password>. Tidak boleh ada user yang melakukan register dengan username yang sama. Kemudian, Buatlah login system yang mencocokkan kredensial antara username dan password.
```
Solution:<br>
```c

// Fungsi untuk menghitung hash MD5 dari string
void compute_md5(const char *input, char *output) {
    char command[1024];
    snprintf(command, sizeof(command), "echo -n '%s' | md5sum", input);

    FILE *fp = popen(command, "r");
    if (fp == NULL) {
        printf("Failed to execute command.\n");
        return;
    }

    if (fgets(output, 33, fp) != NULL) {
        // Remove trailing newline
        output[strcspn(output, "\n")] = '\0';
    }

    pclose(fp);
}

// Fungsi untuk memeriksa apakah username sudah terdaftar
int is_username_registered(const char *username) {
    FILE *file = fopen(user_file, "r");
    if (file == NULL) {
        perror("Failed to open user file");
        exit(EXIT_FAILURE);
    }

    char line[MAX_LINE_LENGTH];
    while (fgets(line, sizeof(line), file) != NULL) {
        char *stored_username = strtok(line, ";");
        if (strcmp(username, stored_username) == 0) {
            fclose(file);
            return 1;  // Username sudah terdaftar
        }
    }

    fclose(file);
    return 0;  // Username belum terdaftar
}

// Fungsi untuk melakukan registrasi username dan password
int register_user(const char *username, const char *password) {
    if (is_username_registered(username)) {
        printf("Failed to register. Username '%s' already exists.\n", username);
        return -1;
    }

    char md5_hash[MD5_DIGEST_LENGTH * 2 + 1];
    compute_md5(password, md5_hash);

    FILE *file = fopen(user_file, "a");
    if (file == NULL) {
        perror("Failed to open user file");
        exit(EXIT_FAILURE);
    }

    fprintf(file, "%s;%s\n", username, md5_hash);
    fclose(file);

    printf("User '%s' registered successfully.\n", username);
    return 0;
}

static int register_user_handler(const char *username, const char *password) {
    return register_user(username, password);
}

static int registerfs_getattr(const char *path, struct stat *stbuf) {
    memset(stbuf, 0, sizeof(struct stat));
    printf("\nhallooooo!!!!\n");
    if (strcmp(path, "/") == 0) {
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_nlink = 2;
    } else {
        return -1;
    }

    return 0;
}
static struct fuse_operations registerfs_operations = {
    .getattr = registerfs_getattr
};


```
Explanation:

- `void compute_md5(const char *input, char *output)`: This is a function declaration for a function named `compute_md5`. It takes two parameters: `input`, which is a pointer to a constant character (string), and `output`, which is a pointer to a character (string).
- `char command[1024];`: This declares a character array named `command` with a size of 1024 characters. It will be used to store a command string.
- `snprintf(command, sizeof(command), "echo -n '%s' | md5sum", input);`: This line uses the `snprintf` function to format and store a command string in the `command` array. The command string calculates the MD5 hash of the `input` string using the `md5sum` utility.
- `FILE *fp = popen(command, "r");`: This declares a file pointer variable named `fp` and uses the `popen` function to open a pipe to a process. The `command` string is executed, and the output is directed to the pipe. The mode "r" indicates that the pipe will be used for reading.
- `if (fp == NULL) { ... }`: This condition checks if the `fp` file pointer is `NULL`, indicating an error occurred while executing the command. If `fp` is `NULL`, an error message is printed, and the function returns.
- `if (fgets(output, 33, fp) != NULL) { ... }`: This line reads at most 33 characters from the `fp` pipe and stores them in the `output` string. If successful (i.e., `fgets` doesn't return `NULL`), the code inside the if statement is executed.
- `output[strcspn(output, "\n")] = '\0';`: This line removes the trailing newline character (`'\n'`) from the `output` string. It uses the `strcspn` function to find the position of the newline character and then replaces it with a null character (`'\0'`).
- `pclose(fp);`: This line closes the pipe, releasing system resources associated with it.
- `int is_username_registered(const char *username)`: This is a function declaration for a function named `is_username_registered`. It takes a parameter `username`, which is a pointer to a constant character (string), and returns an integer.
- `FILE *file = fopen(user_file, "r");`: This declares a file pointer variable named `file` and uses the `fopen` function to open a file named `user_file` in read mode ("r"). The `user_file` is likely a variable or constant storing the path to the user file.
- `if (file == NULL) { ... }`: This condition checks if the `file` file pointer is `NULL`, indicating an error occurred while opening the file. If `file` is `NULL`, an error message is printed, and the program exits with a failure status.
- `char line[MAX_LINE_LENGTH];`: This declares a character array named `line` with a size of `MAX_LINE_LENGTH`. It will be used to store a line read from the file.
- `while (fgets(line, sizeof(line), file) != NULL) { ... }`: This loop reads lines from the `file` until there are no more lines (i.e., `fgets` returns `NULL`). Each line is stored in the `line` array.
- `char *stored_username = strtok(line, ";");`: This line uses the `strtok` function to tokenize the `line` string using the delimiter `";"`. The first token (username) is stored in the `stored_username` pointer.
- `if (strcmp(username, stored_username) == 0) { ... }`: This condition compares the `username` string with the `stored_username` string using the `strcmp` function. If they are equal (i.e., `strcmp` returns 0), the code inside the if statement is executed.
- `fclose(file);`: This line closes the `file`, releasing system resources associated with it.
- `return 1;  // Username sudah terdaftar`: This line returns the value 1, indicating that the username is already registered.
- `fclose(file);`: This line closes the `file`, releasing system resources associated with it.
- `return 0;  // Username belum terdaftar`: This line returns the value 0, indicating that the username is not registered.
- `int register_user(const char *username, const char *password)`: This is a function declaration for a function named `register_user`. It takes two parameters: `username`, which is a pointer to a constant character (string), and `password`, which is a pointer to a constant character (string). It returns an integer.
- `if (is_username_registered(username)) { ... }`: This condition checks if the `is_username_registered` function returns a non-zero value for the given `username`. If the username is already registered, an error message is printed, and the function returns -1.
- `char md5_hash[MD5_DIGEST_LENGTH * 2 + 1];`: This line declares a character array named `md5_hash` with a size of `MD5_DIGEST_LENGTH * 2 + 1`. It will be used to store the MD5 hash.
- `compute_md5(password, md5_hash);`: This line calls the `compute_md5` function, passing the `password` string and `md5_hash` array as arguments. It computes the MD5 hash of the password and stores it in the `md5_hash` array.
- `FILE *file = fopen(user_file, "a");`: This declares a file pointer variable named `file` and uses the `fopen` function to open the `user_file` in append mode ("a"). It means that new data will be appended to the file if it exists, or a new file will be created if it doesn't exist.
- `if (file == NULL) { ... }`: This condition checks if the `file` file pointer is `NULL`, indicating an error occurred while opening the file. If `file` is `NULL`, an error message is printed, and the program exits with a failure status.
- `fprintf(file, "%s;%s\n", username, md5_hash);`: This line writes formatted data to the `file`. It writes the `username`, a semicolon (`;`), the `md5_hash`, and a newline character (`\n`) to the file.
- `fclose(file);`: This line closes the `file`, releasing system resources associated with it.
- `printf("User '%s' registered successfully.\n", username);`: This line prints a success message to the console, indicating that the user has been registered successfully.
- `return 0;`: This line returns the value 0, indicating that the registration was successful.
- `static int register_user_handler(const char *username, const char *password) { ... }`: This is a static function declaration for a function named `register_user_handler`. It takes two parameters: `username`, which is a pointer to a constant character (string), and `password`, which is a pointer to a constant character (string). It returns an integer.
- `return register_user(username, password);`: This line calls the `register_user` function, passing the `username` and `password` strings as arguments, and returns its return value.
- `static int registerfs_getattr(const char *path, struct stat *stbuf) { ... }`: This is a static function declaration for a function named `registerfs_getattr`. It takes two parameters: `path`, which is a pointer to a constant character (string), and `stbuf`, which is a pointer to a `struct stat`. It returns an integer.
- `memset(stbuf, 0, sizeof(struct stat));`: This line uses the `memset` function to set the `stbuf` memory to zero. It initializes the `stbuf` structure to ensure all its members are initialized properly.
- `printf("\nhallooooo!!!!\n");`: This line prints a message to the console. It is likely a debug statement or placeholder for further implementation.
- `if (strcmp(path, "/") == 0) { ... }`: This condition checks if the `path` string is equal to `"/"`. If it is, the code inside the if statement is executed.
- `stbuf->st_mode = S_IFDIR | 0755;`: This line sets the `st_mode` member of the `stbuf` structure to a directory mode value. It is a bitwise OR operation between `S_IFDIR` (directory flag) and the octal value `0755` (permissions).
- `stbuf->st_nlink = 2;`: This line sets the `st_nlink` member of the `stbuf` structure to 2, indicating that the directory has two links (itself and the parent directory).
- `return 0;`: This line returns the value 0, indicating success.
- `static struct fuse_operations registerfs_operations = { ... }`: This line declares a static structure variable named `registerfs_operations` of type `struct fuse_operations`. It is used to define the operations for a FUSE (Filesystem in Userspace) file system.
- `.getattr = registerfs_getattr`: This line assigns the `registerfs_getattr` function to the `getattr` member of the `registerfs_operations` structure. It associates the `getattr` operation with the corresponding function.

In summary, the code defines several functions related to user registration and file system operations. It includes functions for calculating the MD5 hash, checking if a username is already registered, registering a new user, and implementing file system operations for a FUSE-based file system.

### 5D
```
Folder tersebut hanya dapat diakses oleh user yang telah melakukan login. User yang login dapat membaca folder dan file yang di-mount kemudian dengan menggunakan FUSE lakukan rename pada folder menjadi <Nama_Folder>_<Kode_Kelompok> dan <Kode_Kelompok>_<Nama_File>.<ext>.

```
Explanation:

### 5E
```
List seluruh folder, subfolder, dan file yang telah di-rename dalam file result.txt menggunakan tree kemudian hitung file tersebut berdasarkan extension dan output-kan menjadi extension.txt.
```
Solution:<br>
```c

```
Explanation:
