#define FUSE_USE_VERSION 30
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/md5.h>
#include <sys/stat.h>
#define MAX_USERNAME_LENGTH 20
#define MAX_PASSWORD_LENGTH 20
#define MAX_LINE_LENGTH (MAX_USERNAME_LENGTH + MD5_DIGEST_LENGTH * 2 + 2)

static const char *user_file = "users.txt";

void download_n_extract() {
    const char* downloadLink = "https://drive.google.com/uc?export=download&id=18YCFdG658SALaboVJUHQIqeamcfNY39a&confirm=t&uuid=38b0d9df-d8cc-46c8-9f03-b455b6f12ed9&at=AKKF8vzt0K3vcrmKQYVrFdCQpjzY:1685177895570";
    const char* fileName = "rahasia.zip";

    // Download the file using wget
    char downloadCommand[256];
    snprintf(downloadCommand, sizeof(downloadCommand), "wget -O %s \"%s\"", fileName, downloadLink);
    int downloadStatus = system(downloadCommand);
    if (downloadStatus != 0) {
        printf("Failed to download the file.\n");
        return;
    }

    // Extract the downloaded file using unzip
    char extractCommand[256];
    snprintf(extractCommand, sizeof(extractCommand), "unzip %s", fileName);
    int extractStatus = system(extractCommand);
    if (extractStatus != 0) {
        printf("Failed to extract the file.\n");
        return;
    }

    printf("File downloaded and extracted successfully.\n");
}

void mount() {
    system("sudo docker run -it --name temp_container ubuntu:focal");
    system("apt-get update");
    system("sudo docker commit temp_container rahasia_di_docker_u02");
    system("exit");
    system("sudo docker run -v $(pwd)/rahasia:/usr/share/rahasia -it rahasia_di_docker_u02");
}


// Fungsi untuk menghitung hash MD5 dari string
void compute_md5(const char *input, char *output) {
    char command[1024];
    snprintf(command, sizeof(command), "echo -n '%s' | md5sum", input);

    FILE *fp = popen(command, "r");
    if (fp == NULL) {
        printf("Failed to execute command.\n");
        return;
    }

    if (fgets(output, 33, fp) != NULL) {
        // Remove trailing newline
        output[strcspn(output, "\n")] = '\0';
    }

    pclose(fp);
}

// Fungsi untuk memeriksa apakah username sudah terdaftar
int is_username_registered(const char *username) {
    FILE *file = fopen(user_file, "r");
    if (file == NULL) {
        perror("Failed to open user file");
        exit(EXIT_FAILURE);
    }

    char line[MAX_LINE_LENGTH];
    while (fgets(line, sizeof(line), file) != NULL) {
        char *stored_username = strtok(line, ";");
        if (strcmp(username, stored_username) == 0) {
            fclose(file);
            return 1;  // Username sudah terdaftar
        }
    }

    fclose(file);
    return 0;  // Username belum terdaftar
}

// Fungsi untuk melakukan registrasi username dan password
int register_user(const char *username, const char *password) {
    if (is_username_registered(username)) {
        printf("Failed to register. Username '%s' already exists.\n", username);
        return -1;
    }

    char md5_hash[MD5_DIGEST_LENGTH * 2 + 1];
    compute_md5(password, md5_hash);

    FILE *file = fopen(user_file, "a");
    if (file == NULL) {
        perror("Failed to open user file");
        exit(EXIT_FAILURE);
    }

    fprintf(file, "%s;%s\n", username, md5_hash);
    fclose(file);

    printf("User '%s' registered successfully.\n", username);
    return 0;
}

static int register_user_handler(const char *username, const char *password) {
    return register_user(username, password);
}

static int registerfs_getattr(const char *path, struct stat *stbuf) {
    memset(stbuf, 0, sizeof(struct stat));
    printf("\nhallooooo!!!!\n");
    if (strcmp(path, "/") == 0) {
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_nlink = 2;
    } else {
        return -1;
    }

    return 0;
}
static struct fuse_operations registerfs_operations = {
    .getattr = registerfs_getattr
};

int main(int argc, char *argv[]) {
    download_n_extract();
    mount();

     if (argc != 2) {
        printf("Usage: %s <mountpoint>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    FILE *file = fopen(user_file, "a");
    if (file == NULL) {
        perror("Failed to create user file");
        exit(EXIT_FAILURE);
    }
    fclose(file);

    register_user("admin", "password");

    argv[0] = "";  // Set the first argument to an empty string for FUSE
    return fuse_main(argc, argv, &registerfs_operations, NULL);
}
