#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <sys/wait.h>
#include <unistd.h>

#define MAX_COLUMNS 29
#define MAX_LINE_LENGTH 1000

typedef struct {
    int id;
    const char* name;
    int age;
    const char* photoURL;
    const char* nationality;
    int potential;
    const char* club;
} Player;

void downloadDataset() {
    const char* downloadCommand = "kaggle datasets download -d bryanb/fifa-player-stats-database";
    system(downloadCommand);
}

void extractDataset() {
    const char* unzipCommand = "unzip fifa-player-stats-database.zip";
    system(unzipCommand);
}

bool isInterestedClub(const char* club) {
    // Club filtering 
    return strcmp(club, "Manchester City") != 0;
}

void readCSV(const char* filename) {
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("File is not found!\n");
        return;
    }

    // Read the header line
    char header[MAX_LINE_LENGTH];
    fgets(header, sizeof(header), file);

    // Read and process each line of the CSV file
    char line[MAX_LINE_LENGTH];
    while (fgets(line, sizeof(line), file)) {
        char* token = strtok(line, ",");
        int column = 1;
        Player player;

        while (token != NULL && column <= MAX_COLUMNS) {
            switch (column) {
                case 1:
                    player.id = atoi(token);
                    break;
                case 2:
                    player.name = token;
                    break;
                case 3:
                    player.age = atoi(token);
                    break;
                case 4:
                    player.photoURL = token;
                    break;
                case 5:
                    player.nationality = token;
                    break;
                case 8:
                    player.potential = atoi(token);
                    break;
                case 9:
                    player.club = token;
                    break;
            }

            token = strtok(NULL, ",");
            column++;
        }

        if (player.age < 25 && player.potential > 85 && isInterestedClub(player.club)) {
            printf("Id: %d\n", player.id);
            printf("Name: %s\n", player.name);
            printf("Age: %d\n", player.age);
            printf("Photo: %s\n", player.photoURL);
            printf("Nationality: %s\n", player.nationality);
            printf("Potential: %d\n", player.potential);
            printf("Club: %s\n", player.club);
            printf("----------------------------------\n");
        }
    }

    fclose(file);
}

int main() {
    downloadDataset();
    extractDataset();

    // Create a child process to execute readCSV
    pid_t child_pid = fork();

    if (child_pid == -1) {
        // Error occurred
        perror("Fork failed");
        exit(EXIT_FAILURE);
    } else if (child_pid == 0) {
        // Child process executes readCSV
        readCSV("FIFA23_official_data.csv");
        exit(EXIT_SUCCESS);
    } else {
        // Parent process waits for the child process to complete
        int status;
        waitpid(child_pid, &status, 0);
    }

    return 0;
}